import 'package:climbing_party_app/model/goods_item_latest.dart';
import 'package:flutter/material.dart';

class ComponentItemLatest extends StatelessWidget {
  const ComponentItemLatest({
    super.key,

    required this.goodsItemLatest,
    required this.callback,
  });

  final GoodsItemLatest goodsItemLatest;
  final VoidCallback callback;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Container(
            height: 550,
            margin: EdgeInsets.fromLTRB(0, 20, 20, 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12.withOpacity(0.06),
                    spreadRadius: 5,
                    blurRadius: 20,
                    offset: Offset(0,0)
                )
              ],
            ),
            child: Column(
              children: [
                Container(
                  alignment: FractionalOffset.centerLeft,
                  margin: EdgeInsets.fromLTRB(20, 40, 20, 10),
                  child: Column(
                    children: [
                      Text(
                        goodsItemLatest.goodsLatestTitle,
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                          fontSize: 23,
                          color: Colors.black,
                          letterSpacing: -1.0,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: FractionalOffset.centerLeft,
                  margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
                  child: Column(
                    children: [
                      Text(
                        goodsItemLatest.goodsLatestSentence,
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                          fontSize: 13,
                          color: Colors.black,
                          letterSpacing: -1.0,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: Image.asset(
                    goodsItemLatest.imgUrl,
                    fit: BoxFit.cover,
                  ),
                  width: 300,
                  height: 300,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
