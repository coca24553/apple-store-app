import 'package:climbing_party_app/model/goods_item.dart';
import 'package:flutter/material.dart';

class ComponentItems extends StatelessWidget {
  const ComponentItems({
    super.key,
    required this.goodsItem,
    required this.callback
  });

  final GoodsItem goodsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Container(
            width: 180, height: 180,
            alignment: FractionalOffset.centerLeft,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12.withOpacity(0.06),
                    spreadRadius: 10,
                    blurRadius: 30,
                    offset: Offset(0,0)
                )
              ],
            ),
            child: Column(
              children: [
                Container(
                  child: Column(
                    children: [
                      Container(
                        alignment: FractionalOffset.centerLeft,
                        margin: EdgeInsets.fromLTRB(25, 25, 0, 0),
                        child: Column(
                          children: [
                            Text(
                              goodsItem.goodsTitle,
                              style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                fontSize: 18,
                                color: Colors.black,
                                letterSpacing: -1.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Center(
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                              width: 90,
                              child: Column(
                                children: [
                                  Image.asset(goodsItem.imgUrl)
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
