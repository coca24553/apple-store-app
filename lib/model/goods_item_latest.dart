class GoodsItemLatest {
  num id;
  String goodsLatestTitle;
  String goodsLatestSentence;
  String imgUrl;

  GoodsItemLatest(this.id, this.goodsLatestTitle, this.goodsLatestSentence, this.imgUrl);
}