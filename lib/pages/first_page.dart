import 'package:climbing_party_app/model/goods_item.dart';
import 'package:flutter/material.dart';


class FirstPage extends StatefulWidget {
  const FirstPage({
    super.key,
    required this.goodsItem
  });

  final GoodsItem goodsItem;

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.black,
        elevation: 0,
        title: Text(
          'Apple Store',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
        leading: IconButton(
          icon: Image.asset("assets/apple_logo.png", width: 35, height: 35,),
          onPressed: () {
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Text('${widget.goodsItem.id}'),
            Text(widget.goodsItem.imgUrl),
            Text(widget.goodsItem.goodsTitle)
          ],
        ),
      ),
    );
  }
}


