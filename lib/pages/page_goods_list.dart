import 'package:climbing_party_app/components/component_items.dart';
import 'package:climbing_party_app/model/goods_item.dart';
import 'package:climbing_party_app/pages/first_page.dart';
import 'package:flutter/material.dart';

class PageGoodsList extends StatefulWidget {
  const PageGoodsList({super.key});

  @override
  State<PageGoodsList> createState() => _PageGoodsListState();
}

SliverGridDelegate _sliverGridDelegate() {
  return const SliverGridDelegateWithFixedCrossAxisCount(
      crossAxisCount: 2,
    crossAxisSpacing: 10, //수직padding
    mainAxisSpacing: 3, //수평padding
  );
}

class _PageGoodsListState extends State<PageGoodsList> {
  List<GoodsItem> _list = [
    GoodsItem(1, 'Mac', 'assets/shopping_02/P_1_Mac.PNG'),
    GoodsItem(2, 'iPhone', 'assets/shopping_02/P_2_iPone.PNG'),
    GoodsItem(3, 'iPad', 'assets/shopping_02/P_3_iPad.PNG'),
    GoodsItem(4, 'Apple Watch', 'assets/shopping_02/P_4_Apple Watch.PNG'),
    GoodsItem(5, 'Apple TV 4K', 'assets/shopping_02/P_5_Apple TV 4K.PNG'),
    GoodsItem(6, 'Air Pods', 'assets/shopping_02/P_6_AirPods.PNG'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.builder(
        itemCount: _list.length,
        gridDelegate: _sliverGridDelegate(),
        itemBuilder: (BuildContext ctx, int idx) {
          return ComponentItems(
              goodsItem: _list[idx],
              callback: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => FirstPage(goodsItem: _list[idx],)));
              }
          );
        },
      )
    );
  }
}


