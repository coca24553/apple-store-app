import 'package:climbing_party_app/components/component_item_latest.dart';
import 'package:climbing_party_app/components/component_items.dart';
import 'package:climbing_party_app/model/goods_item_latest.dart';
import 'package:climbing_party_app/pages/first_page.dart';
import 'package:flutter/material.dart';
import 'package:climbing_party_app/model/goods_item.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

SliverGridDelegate _sliverGridDelegate() {
  return const SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: 2,
    crossAxisSpacing: 2,
    mainAxisSpacing: 5,
  );
}

class _PageIndexState extends State<PageIndex> {
  List<GoodsItem> _list = [
    GoodsItem(1, 'Mac', 'assets/shopping_02/P_1_Mac.PNG'),
    GoodsItem(2, 'iPhone', 'assets/shopping_02/P_2_iPone.PNG'),
    GoodsItem(3, 'iPad', 'assets/shopping_02/P_3_iPad.PNG'),
    GoodsItem(4, 'Apple Watch', 'assets/shopping_02/P_4_Apple Watch.PNG'),
    GoodsItem(5, 'Apple TV 4K', 'assets/shopping_02/P_5_Apple TV 4K.PNG'),
    GoodsItem(6, 'Air Pods', 'assets/shopping_02/P_6_AirPods.PNG'),
  ];

  List<GoodsItemLatest> _listLatest = [
    GoodsItemLatest(1, '대학생활용 Mac 또는 iPad를 보다 저렴하게.', '여기에 Mac 구입 시 AirPods 제공, iPad 구입 시 Apple Pecil 제공, AppleCare+ 20% 할인 등 다양한 혜택을 누릴 수 있습니다.', 'assets/latest_03/latest_01.PNG'),
    GoodsItemLatest(2, 'iPad', '매일매일 여러 일을 척척. 널찍한 화면. 화사한 색상.', 'assets/latest_03/latest_04.PNG'),
    GoodsItemLatest(3, 'MacBook Pro 14 및 16', '막강한 성능의 M3, M3 Pro 또는 M3 Max 칩 탑재, 최대 22시간의 배터리 사용 시간.※ 놀라운 밝기를 자랑하는 XDR 디스플레이까지. 지금 살펴보기.', 'assets/latest_03/latest_03.PNG')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.black,
        elevation: 0,
        title: Text(
          'Apple Store',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
        leading: IconButton(
          icon: Image.asset("assets/apple_logo.png", width: 35, height: 35,),
          onPressed: () {
          },
        ),
        actions: [
          IconButton(
            icon: Icon(
                Icons.shopping_cart,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {},
          ),
        ],
      ),

      body:
      SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.black,
              child: Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Container(
                            child: Column(
                              children: [
                                Container(
                                  alignment: FractionalOffset.centerLeft,
                                  padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                                  child: Column(
                                    children: [
                                      Text(
                                        'NEW',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                            fontSize: 18,
                                            color: Colors.grey[500]
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  alignment: FractionalOffset.centerLeft,
                                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  child: Column(
                                    children: [
                                      Text(
                                        'iPone 15 Pro',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_Bold',
                                            fontSize: 36,
                                            color: Colors.white,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  alignment: FractionalOffset.centerLeft,
                                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                  child: Column(
                                    children: [
                                      Text(
                                        '초강력, 초경량. 완전히 새로운 티타늄 디자인, Apple 사상'
                                            '\n가장 앞선 48MP 메인 카메라, 그리고 게임의 판도를 바꾸는'
                                            '\nA17 Pro 지금 자세히 알아보세요.',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                            fontSize: 16,
                                            color: Colors.grey[500]
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/main_01/main_15pro.png',
                            width: 190,
                          )
                        ],
                      ),
                    ),
                  ]
              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    alignment: FractionalOffset.centerLeft,
                    child: Column(
                      children: [
                        Text(
                          '제품별로 쇼핑하기',
                          style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: 20,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        GridView.builder(
                          itemCount: _list.length,
                          gridDelegate: _sliverGridDelegate(),
                          itemBuilder: (BuildContext ctx, int idx){
                            return ComponentItems(
                              goodsItem: _list[idx],
                              callback: () {
                                Navigator.of(context).push(
                                    MaterialPageRoute(
                                        builder: (context) => FirstPage(goodsItem: _list[idx])));
                              },
                            );
                          },
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.white,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    alignment: FractionalOffset.centerLeft,
                    child: Column(
                      children: [
                        Text('최신 제품',
                          style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: 20,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 700,
                    height: 600,
                    child: PageView.builder(
                      controller: PageController(
                        initialPage: 0,
                        viewportFraction: 0.9,
                      ),
                      itemBuilder: (BuildContext context, int idx){
                        return ComponentItemLatest(
                            goodsItemLatest: _listLatest[idx],
                            callback: () {}
                        );
                      },
                      itemCount: _listLatest.length,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
